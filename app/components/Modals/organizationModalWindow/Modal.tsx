import * as React from 'react';
import './Modal.less';

 const Modal =(props:any)=>{
    const isOpen= props.isModalOpen;
     if (isOpen === false) return null;


     let onNameChange = (e:any)=>{
         let body = e.target.value;
         props.actions.changeOrgName(body);
     };
     let nameChanged = props.name;

     let onAddressChange = (e:any)=>{
         let body = e.target.value;
         props.actions.changeOrgAddress(body);
     };
     let addressChanged = props.address;

     let onINNChange = (e:any)=>{
         let body = e.target.value;
         props.actions.changeOrgINN(body);
     };
     let INNChanged = props.INN;

     let createOrganization =()=>{
         let body = {
             name: props.name,
             address:props.address,
             INN:props.INN
         };
         props.actions2.createOrganization(body);
         props.actions.closeOrgModal();
     };
     let editOrganization =()=>{
         let body = {
             name: props.name,
             address:props.address,
             INN:props.INN,
             id: props.id_organization
         };
         props.actions2.editOrganization(body);
         props.actions.closeOrgModal();
     };
     let closeModal =()=>{
         props.actions.closeOrgModal();
     };
     let onClickDeleteOrg =()=>{
         props.actions2.deleteOrganization(props.id_organization);
         props.actions.closeOrgModal();
     };
    return(  <div>
            {props.isTypeModalNumber === 1 ?
        <div className="modal">
                    <span className="close" onClick={closeModal}>&times;</span>
                    <h5>Изменить организацию:</h5>
                    <div>
                        <input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={nameChanged}
                         onChange={onNameChange}
                        placeholder="Organization name"
                    /></div>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={addressChanged}
                        onChange={onAddressChange}
                        placeholder="Organization address"
                    /></div>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={INNChanged}
                        onChange={onINNChange}
                        placeholder="Organization INN"
                    /></div>
                    <div>
                        <button onClick={editOrganization}>Изменить</button>
                    </div>
                </div>
                :props.isTypeModalNumber === 2 ?
                <div className="modal">
                    <span className="close" onClick={closeModal}>&times;</span>
                    <h5>Добавить организацию:</h5>
                    <div>
                        <input
                            className="btn-password "
                            // disabled={waitingForLogin}
                            type="text"
                            value={nameChanged}
                            onChange={onNameChange}
                            placeholder="Organization name"
                        /></div>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={addressChanged}
                        onChange={onAddressChange}
                        placeholder="Organization address"
                    /></div>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={INNChanged}
                        onChange={onINNChange}
                        placeholder="Organization INN"
                    /></div>
                    <div>
                        <button onClick={createOrganization}>Добавить</button>
                    </div>
                </div>
                :props.isTypeModalNumber === 3?
                <div className="modal">
                <span className="close" onClick={closeModal}>&times;</span>
                <h5>Вы действительно хотите удалить данные этой организации?</h5>
                <div>
                <button onClick={onClickDeleteOrg}>Да, удалить!</button>
                <button onClick={closeModal}>Отмена</button>
                </div>
                </div>
                :null}
        </div>
    )
 }

 export {Modal};
