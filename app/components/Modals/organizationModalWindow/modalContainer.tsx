import React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {organizationsActions} from '../../../Actions/organizationsActions/organizationsActions';
import {IDispatchProps, IStateProps} from '../../../App/App';
import {IActionType} from '../../../common';
import {IStoreState} from '../../../Reducers/organizationModalReducer';
import {Modal} from './Modal';
import {OrganizationModalWindowActions} from '../../../Actions/modalActions/organizationModalWindowActions/organizationModalWindowActions';

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        isModalOpen: state.organizationModalReducer.isModalOpen,
        isTypeModalNumber: state.organizationModalReducer.isTypeModalNumber,
        id: state.organizationModalReducer.id,
        id_organization: state.organizationModalReducer.id_organization,
        name: state.organizationModalReducer.name,
        address: state.organizationModalReducer.address,
        INN: state.organizationModalReducer.INN,

    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>) {

    return {
        actions: new OrganizationModalWindowActions(dispatch),
        actions2: new organizationsActions(dispatch),
    }
}

const ModalContainer = connect(mapStateToProps, mapDispatchToProps)(Modal);

export {ModalContainer};
