import * as React from 'react';
import './Modal.less';

 const Modal =(props:any)=>{
    const isOpen= props.isModalOpen;
     if (isOpen === false) return null;

     let onFIOChange = (e:any)=>{
         let body = e.target.value;
         props.actions.changeEmpFIO(body);
     };
     let FIOChanged = props.FIO;

     let onAddressChange = (e:any)=>{
         let body = e.target.value;
         props.actions.changeEmpAddress(body);
     };
     let addressChanged = props.address;

     let onPositionChange = (e:any)=>{
         let body = e.target.value;
         props.actions.changeEmpPosition(body);
     };
     let positionChanged = props.position;

     let createEmployee =()=>{
         let body = {
             id_division:props.id_division,
             FIO: props.FIO,
             address: props.address,
             position: props.position,
         };
         props.actions2.createEmployee(body);
         props.actions.closeEmpModal();
     };
     let editEmployee =()=>{
         let body = {
             FIO: props.FIO,
             address: props.address,
             position: props.position,
             id: props.id_employee
         };
         props.actions2.editEmployee(body);
         props.actions.closeEmpModal();
     };
     let closeModal =()=>{
         props.actions.closeEmpModal();
     };
     let onClickDeleteEmp =()=>{
         props.actions2.deleteEmployee(props.id_employee,props.id_division);
         props.actions.closeEmpModal();
     }
    return(  <div>
            {props.isTypeModalNumber === 1 ?
        <div className="modal">
                    <span className="close" onClick={closeModal}>&times;</span>
                    <h5>Изменить данные сотрудника:</h5>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={FIOChanged}
                        onChange={onFIOChange}
                        placeholder="Employee FIO"
                    /></div>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={addressChanged}
                        onChange={onAddressChange}
                        placeholder="Employee address"
                    /></div>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={positionChanged}
                        onChange={onPositionChange}
                        placeholder="Employee position"
                    /></div>
                    <div>
                        <button onClick={editEmployee}>Изменить</button>
                    </div>
                </div>
                :props.isTypeModalNumber === 2 ?
                <div className="modal">
                    <span className="close" onClick={closeModal}>&times;</span>
                    <h5>Добавить сотрудника:</h5>
                    <div><input
                            className="btn-password "
                            // disabled={waitingForLogin}
                            type="text"
                            value={FIOChanged}
                            onChange={onFIOChange}
                            placeholder="Employee FIO"
                        /></div>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={addressChanged}
                        onChange={onAddressChange}
                        placeholder="Employee address"
                    /></div>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={positionChanged}
                        onChange={onPositionChange}
                        placeholder="Employee position"
                    /></div>
                    <div>
                        <button onClick={createEmployee}>Добавить</button>
                    </div>
                </div>

                    :props.isTypeModalNumber === 3?
                <div className="modal">
                <span className="close" onClick={closeModal}>&times;</span>
                <h5>Вы действительно хотите удалить данные этого сотрудника?</h5>
                <div>
                <button onClick={onClickDeleteEmp}>Да, удалить!</button>
                <button onClick={closeModal}>Отмена</button>
                </div>
                </div>
            :null}
        </div>
    )
 }

 export {Modal};
