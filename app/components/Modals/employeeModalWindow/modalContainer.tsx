import React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {IDispatchProps, IStateProps} from '../../../App/App';
import {IActionType} from '../../../common';
import {IStoreState} from '../../../Reducers/employeeModalReducer';
import {Modal} from './Modal';
import {employeeActions} from '../../../Actions/employeeActions/employeeActions';
import {EmployeeModalWindowActions} from '../../../Actions/modalActions/employeeModalWindowActions/employeeModalWindowActions';

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        isModalOpen: state.employeeModalReducer.isModalOpen,
        id_employee: state.employeeModalReducer.id_employee,
        FIO: state.employeeModalReducer.FIO,
        address: state.employeeModalReducer.address,
        position: state.employeeModalReducer.position,
        id_division: state.employeeModalReducer.id_division,
        isTypeModalNumber: state.employeeModalReducer.isTypeModalNumber,

    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>) {

    return {
        actions: new EmployeeModalWindowActions(dispatch),
        actions2: new employeeActions(dispatch),
    }
}

const ModalContainer = connect(mapStateToProps, mapDispatchToProps)(Modal);

export {ModalContainer};
