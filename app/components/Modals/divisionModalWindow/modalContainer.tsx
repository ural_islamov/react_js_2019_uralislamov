import React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {divisionsActions} from '../../../Actions/divisionsActions/divisionsActions';
import {IDispatchProps, IStateProps} from '../../../App/App';
import {IActionType} from '../../../common';
import {IStoreState} from '../../../Reducers/divisionModalReducer';
import {Modal} from './Modal';
import {DivisionModalWindowActions} from '../../../Actions/modalActions/divisionModalWindowActions/divisionModalWindowActions';

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        isModalOpen: state.divisionModalReducer.isModalOpen,
        name: state.divisionModalReducer.name,
        phone: state.divisionModalReducer.phone,
        id_organization: state.divisionModalReducer.id_organization,
        id_division: state.divisionModalReducer.id_division,
        isTypeModalNumber: state.divisionModalReducer.isTypeModalNumber,

    }
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>) {

    return {
        actions: new DivisionModalWindowActions(dispatch),
        actions2: new divisionsActions(dispatch),
    }
}

const ModalContainer = connect(mapStateToProps, mapDispatchToProps)(Modal);

export {ModalContainer};
