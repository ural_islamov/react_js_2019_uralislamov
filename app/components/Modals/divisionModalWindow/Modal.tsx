import * as React from 'react';
import './Modal.less';

 const Modal =(props:any)=>{
    const isOpen= props.isModalOpen;
     if (isOpen === false) return null;

     let onNameChange = (e:any)=>{
         let body = e.target.value;
         props.actions.changeDivName(body);
     };
     let nameChanged = props.name;

     let onPhoneChange = (e:any)=>{
         let body = e.target.value;
         props.actions.changeDivPhone(body);
     };
     let phoneChange = props.phone;

     let createDivision =()=>{
         let body = {
             id_organization:props.id_organization,
             name: props.name,
             phone:props.phone,
         };
         props.actions2.createDivision(body);
         props.actions.closeDivModal();
     };
     let editDivision =()=>{
         let body = {
             name: props.name,
             phone:props.phone,
             id: props.id_division
         };
         props.actions2.editDivision(body);
         props.actions.closeDivModal();
     };
     let closeModal =()=>{
         props.actions.closeDivModal();
     };
     let onClickDeleteDiv =()=>{
         props.actions2.deleteDivision(props.id_division, props.id_organization);
         props.actions.closeDivModal();
     };
    return(  <div>
            {props.isTypeModalNumber === 1 ?
        <div className="modal">
                    <span className="close" onClick={closeModal}>&times;</span>
                    <h5>Изменить подразделение:</h5>
                    <div>
                        <input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={nameChanged}
                         onChange={onNameChange}
                        placeholder="Division name"
                    /></div>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={phoneChange}
                        onChange={onPhoneChange}
                        placeholder="Division phone"
                    /></div>
                    <div>
                        <button onClick={editDivision}>Изменить</button>
                    </div>
                </div>
                :props.isTypeModalNumber === 2 ?
                <div className="modal">
                    <span className="close" onClick={closeModal}>&times;</span>
                    <h5>Добавить подразделение:</h5>
                    <div>
                        <input
                            className="btn-password "
                            // disabled={waitingForLogin}
                            type="text"
                            value={nameChanged}
                            onChange={onNameChange}
                            placeholder="Division name"
                        /></div>
                    <div><input
                        className="btn-password "
                        // disabled={waitingForLogin}
                        type="text"
                        value={phoneChange}
                        onChange={onPhoneChange}
                        placeholder="Division phone"
                    /></div>
                    <div>
                        <button onClick={createDivision}>Добавить</button>
                    </div>
                </div>
                :props.isTypeModalNumber === 3?
                <div className="modal">
                <span className="close" onClick={closeModal}>&times;</span>
                <h5>Вы действительно хотите удалить данные этого подразделения?</h5>
                <div>
                <button onClick={onClickDeleteDiv}>Да, удалить!</button>
                <button onClick={closeModal}>Отмена</button>
                </div>
                </div>
                :null}
        </div>
    )
 };

 export {Modal};
