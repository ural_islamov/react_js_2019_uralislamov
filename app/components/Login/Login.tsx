import * as React from 'react';
import {Redirect} from 'react-router';

const Login = (props) => {
    let onLoginChange = (e:any)=>{
        let login = e.target.value;
        props.actions.changeLogin(login);
    };
    let loginChanged = props.login;
    let onPasswordChange = (e:any)=>{
        let password = e.target.value;
        props.actions.changePassword(password);
    };
    let passwordChanged = props.password;

    let onClickIn =()=>{
        let body = {
            login: props.login,
            password: props.password
        };
        props.actions.getAuthorization(body);
    };
    let onClickOut =()=>{
        props.actions.outOnWebsite();
    };
    //if(props.isAuthorizeStatus) return <Redirect to={'/organizations'}/>;
    return (
        <div>
            <h5>Авторизация: </h5>
            <div><input
                className="btn-password "
                // disabled={waitingForLogin}
                type="text"
                value={loginChanged}
                onChange={onLoginChange}
                placeholder="Введите логин"
            /></div>
            <div><input
                className="btn-password "
                // disabled={waitingForLogin}
                type="password"
                value={passwordChanged}
                onChange={onPasswordChange}
                placeholder="Введите пороль"
            /></div>
            {props.isAuthorizeStatus? <button onClick={onClickOut}>Выйти</button>:<button onClick={onClickIn}>Войти</button>}
        </div>
    )
};

export {
    Login
};
