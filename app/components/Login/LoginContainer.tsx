import * as axios from 'axios';
import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {IDispatchProps, IStateProps} from '../../App/App';
import {IActionType} from '../../common';
import {IStoreState} from '../../Reducers/LoginReducer';
import {Login} from './Login';
import {LoginActions} from '../../Actions/LoginActions/loginActions'

class LoginContainer extends React.Component{
    render(){
        return (
           <Login login={this.props.login}
                  password={this.props.password}
                  isAuthorizeStatus={this.props.isAuthorizeStatus}
                  actions={this.props.actions}
                 />
        )
    }
}



function mapStateToProps(state: IStoreState): IStateProps {
    return {
        login: state.LoginReducer.login,
        password: state.LoginReducer.password,
        isAuthorizeStatus: state.LoginReducer.isAuthorizeStatus,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {

    return {
        actions: new LoginActions(dispatch),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);

