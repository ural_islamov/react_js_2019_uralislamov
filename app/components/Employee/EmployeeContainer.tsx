import * as axios from 'axios';
import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {employeeActions} from '../../Actions/employeeActions/employeeActions';
import {IDispatchProps, IStateProps} from '../../App/App';
import {IActionType} from '../../common';
import {IStoreState} from '../../Reducers/employeeReducer';
import {Employee} from './Employee';
import {withRouter} from 'react-router-dom';
import {EmployeeModalWindowActions} from '../../Actions/modalActions/employeeModalWindowActions/employeeModalWindowActions';

class EmployeeContainer extends React.Component{
    componentDidMount() {
        let id:any = this.props.match.params.id;
        if(!id){
            id = 1;
        }
        this.props.actions.setEmployee(id);
        }
    render(){
        return (
           <Employee employee={this.props.employee}
                     actions={this.props.actions}
                     actions2={this.props.actions2}
                   id_division = {this.props.match.params.id}/>
        )
    }
}



function mapStateToProps(state: IStoreState): IStateProps {
    return {
        employee: state.employeeReducer.employee,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {

    return {
        actions: new employeeActions(dispatch),
        actions2: new EmployeeModalWindowActions(dispatch),
    }
}
let WithUrlDataContainerComponent = withRouter(EmployeeContainer);
export default connect(mapStateToProps, mapDispatchToProps)(WithUrlDataContainerComponent);

