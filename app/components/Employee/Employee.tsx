import * as React from 'react';
import {NavLink} from 'react-router-dom';
import {ModalContainer} from '../Modals/employeeModalWindow/modalContainer';


const Employee=(props)=>{
    let onClickAdd = () => {
        let body:any ={id_division:props.id_division, isTypeModalNumber:2};
        props.actions2.openEmpModal(body);
    };
    let onClickDelete =(id:any)=>{
        let body ={id_employee:id, id_division:props.id_division, isTypeModalNumber:3};
        props.actions2.openEmpModalForDelete(body);
    };
    let onClickEdit =(id:any)=>{
        let body ={id_employee:id, id_division:props.id_division, isTypeModalNumber:1};
        props.actions2.getEmployee(body);
    };
       return (
           <div>
               <h5>Сотрудники: </h5> {props.employee.map((T: any) => <div key={T.id}>
               <NavLink to={'/employee/'+T.id}> <span> {T.FIO} </span></NavLink>
               <button onClick={()=>{onClickEdit( T.id)}}>Изменить</button>
               <button onClick={()=>{onClickDelete(T.id)}}>Удалить</button>
           </div>)}
               <button
                   className="btn btn-outline-primary"
                   onClick={onClickAdd}>Добавить</button>
               <ModalContainer/>
           </div>
       )
};

export {Employee};
