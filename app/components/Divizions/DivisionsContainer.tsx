import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {divisionsActions} from '../../Actions/divisionsActions/divisionsActions';
import {IDispatchProps, IStateProps} from '../../App/App';
import {IActionType} from '../../common';
import {IStoreState} from '../../Reducers/divisionsReducer';
import {Divisions} from './Divisions';
import {withRouter} from 'react-router-dom';
import {DivisionModalWindowActions} from '../../Actions/modalActions/divisionModalWindowActions/divisionModalWindowActions';

class DivisionsContainer extends React.Component{
    componentDidMount() {
        let id:any = this.props.match.params.id;
        if(!id){
            id = 1;
        }
        this.props.actions.setDivisions(id);
        }
    render(){
        return (
           <Divisions divisions={this.props.divisions}
                      actions={this.props.actions}
                      actions2={this.props.actions2}
                        id_organization = {this.props.match.params.id}
           />
        )
    }
}



function mapStateToProps(state: IStoreState): IStateProps {
    return {
        divisions: state.divisionsReducer.divisions,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {

    return {
        actions: new divisionsActions(dispatch),
        actions2: new DivisionModalWindowActions(dispatch),
    }
}
let WithUrlDataContainerComponent = withRouter(DivisionsContainer);
export default connect(mapStateToProps, mapDispatchToProps)(WithUrlDataContainerComponent);

