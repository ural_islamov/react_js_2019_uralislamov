import * as React from 'react';
import {NavLink} from 'react-router-dom';
import {ModalContainer} from '../Modals/divisionModalWindow/modalContainer';


const Divisions=(props)=>{
    let onClickAdd = () => {
        let body = {id_organization:props.id_organization,isTypeModalNumber:2 };
        props.actions2.openDivModalForAdd(body);
    };
    let onClickDelete =(id_division:any)=>{
        let body ={id_division:id_division, id_organization:props.id_organization, isTypeModalNumber:3};
        props.actions2.openDivModalForDelete(body);
    };
    let onClickEdit =(id_division:any)=>{
        let body ={id_organization:props.id_organization,id_division:id_division, isTypeModalNumber:1};
        props.actions2.getDivision(body);
    };
       return (
           <div>
               <h5>Подразделения: </h5> {props.divisions.map((T: any) => <div key={T.id}>
               <NavLink to={'/employee/'+T.id}> <span> {T.name} </span></NavLink>
               <button onClick={()=>{onClickEdit(T.id)}}>Изменить</button>
               <button onClick={()=>{onClickDelete(T.id)}}>Удалить</button>
           </div>)}
              <button
                  className="btn btn-outline-primary"
                  onClick={onClickAdd}>Добавить</button>
               <ModalContainer/>
           </div>
       )
}

export {Divisions};
