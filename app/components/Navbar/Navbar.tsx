import * as React from 'react';
import {NavLink} from 'react-router-dom';

const Navbar =()=>{
    return(
        <nav >
            <div> <NavLink to="/login">Login</NavLink></div>
            <div> <NavLink to="/organizations">Organizations</NavLink></div>
        </nav>
    )
 }

 export {Navbar};
