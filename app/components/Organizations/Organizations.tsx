import * as React from 'react';
import {ModalContainer} from '../Modals/organizationModalWindow/modalContainer';
import {NavLink, Redirect} from 'react-router-dom';

const Organizations = (props: any) => {
    if (props.organizations.length === 0) {
        props.actions.setOrganizations()
    }
    let onClickAdd = () => {
        let isTypeModalNumber:number =2;
        props.actions2.openOrgModalForAdd(isTypeModalNumber);
    };
    let onClickDelete =(id_organization:any)=>{
        let body ={id_organization:id_organization, isTypeModalNumber:3};
        props.actions2.openOrgModalForDelete(body)};
    let onClickEdit =(id_organization:any)=>{
        let body ={id_organization:id_organization, isTypeModalNumber:1};
        props.actions2.getOrganization(body)};
    //if(!props.isAuthorizeStatus) return <Redirect to={'/login'}/>;
    return (
        <div>
            <p>{props.message}</p>
           <h5>Организации: </h5> {props.organizations.map((T: any) => <div key={T.id}>
            <NavLink to={'/division/'+T.id}><span> {T.name} </span></NavLink>
            <button onClick={()=>{onClickEdit(T.id)}}>Изменить</button>
            <button onClick={()=>{onClickDelete(T.id)}}>Удалить</button>

        </div>)}
            <div><input
                className="btn btn-outline-primary"
                //disabled={waitingForLogin}
                type="button"
                value="Добавить"
                onClick={onClickAdd}
            /></div>
            <ModalContainer/>
        </div>
    )
}

export {Organizations};
