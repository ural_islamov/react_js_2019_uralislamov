import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {OrganizationModalWindowActions} from '../../Actions/modalActions/organizationModalWindowActions/organizationModalWindowActions';
import {organizationsActions} from '../../Actions/organizationsActions/organizationsActions';
import {IDispatchProps, IStateProps} from '../../App/App';
import {IActionType} from '../../common';
import {IStoreState} from '../../Reducers/organizationsReducer';
import {Organizations} from './Organizations';


function mapStateToProps(state: IStoreState): IStateProps {
    return {
        organizations: state.organizationsReducer.organizations,
        message: state.organizationsReducer.message,
        isAuthorizeStatus: state.LoginReducer.isAuthorizeStatus,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {

    return {
        actions: new organizationsActions(dispatch),
        actions2: new OrganizationModalWindowActions(dispatch),
    }
}

const OrganizationsContainer = connect(mapStateToProps, mapDispatchToProps)(Organizations);

export {OrganizationsContainer};
