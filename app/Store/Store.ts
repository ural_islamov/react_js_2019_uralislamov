import {applyMiddleware, combineReducers, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {organizationsReducer} from '../Reducers/organizationsReducer';
import {organizationModalReducer} from '../Reducers/organizationModalReducer';
import {divisionsReducer} from '../Reducers/divisionsReducer';
import {employeeReducer} from '../Reducers/employeeReducer';
import {divisionModalReducer} from '../Reducers/divisionModalReducer';
import {employeeModalReducer} from '../Reducers/employeeModalReducer';
import {LoginReducer} from '../Reducers/LoginReducer';

/**
 * Redux хранилище (стор).
 */

let reducers = combineReducers({
    organizationsReducer: organizationsReducer,
    divisionsReducer: divisionsReducer,
    employeeReducer: employeeReducer,
    organizationModalReducer: organizationModalReducer,
    divisionModalReducer: divisionModalReducer,
    employeeModalReducer: employeeModalReducer,
    LoginReducer: LoginReducer,
})
const store = createStore( reducers, composeWithDevTools(applyMiddleware(thunk)));

export {store as appStore};
