import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/divisionsActions/divisionsConsts';

/**
 * Состояние для Redux хранилища (стора).
 * @prop {any} divisions Массив для хранения подразделений при запросе с сервера.

 */
export interface IStoreState{
    divisions:any;
    divisionsReducer:any;
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreState {
        return {
            divisions:[],
        };
    }
};

export function divisionsReducer(state: IStoreState = initialState.state, action: IActionType) {
    switch (action.type) {
        case ActionTypes.SET_DIVISIONS:
            return{
                ...state,
                divisions: action.payload
            };
    }
    return state;
}

