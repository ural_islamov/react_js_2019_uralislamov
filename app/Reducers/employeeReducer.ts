import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/employeeActions/employeeConsts';

/**
 * Состояние для Redux хранилища (стора).
 * @prop {boolean} employee Массив сотрудников.
 */
export interface IStoreState{
    employee:any;
    employeeReducer:any;
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreState {
        return {
            employee:[],
        };
    }
};

export function employeeReducer(state: IStoreState = initialState.state, action: IActionType) {
    switch (action.type) {
        case ActionTypes.SET_EMPLOYEE:
            return{
                ...state,
                employee: action.payload
            };
    }
    return state;
}

