import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/organizationsActions/organizationsConsts';

/**
 * Состояние для Redux хранилища (стора).
 * @prop {boolean} organizations Массив организаций.
 * @prop {boolean} message Для хранения сообщения, если нет организаций на сервере.
 */
export interface IStoreState{
    organizations:any;
    organizationsReducer:any;
    message: string;
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreState {
        return {
            organizations:[],
            message: '',
        };
    }
};

export function organizationsReducer(state: IStoreState = initialState.state, action: IActionType) {
    switch (action.type) {
        case ActionTypes.SET_ORGANIZATIONS:
            return{
                ...state,
                organizations: action.payload,
                message:'',
            };
        case ActionTypes.NOT_ORGANIZATIONS:
            return{
                ...state,
                message: 'Пока нет добавленных организаций.',};
    }
    return state;
}

