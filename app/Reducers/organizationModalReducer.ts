import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/modalActions/organizationModalWindowActions/organizationModalWindowConsts';

/**
 * Состояние для Redux хранилища (стора).
 * @prop {boolean} isModalOpen Состояние открытия модального окна.
 * @prop {boolean} isTypeModalNumber  Состояние открытия модального окна в зависимости от нужной операции.
 */
export interface IStoreState {
    isModalOpen: boolean;
    isTypeModalNumber: number;
    id_organization: number;
    name: string;
    address: string;
    INN: string;
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreState {
        return {
            isModalOpen: false,
            isTypeModalNumber: null,
            id_organization: null,
            name: '',
            address: '',
            INN: ''
        };
    }
};

export function organizationModalReducer(state: IStoreState = initialState.state, action: IActionType) {
    switch (action.type) {
        case ActionTypes.OPEN_ORG_MODAL_FOR_ADD:
            return {
                ...state,
                isModalOpen: true,
                isTypeModalNumber: action.payload,
            };
        case ActionTypes.CLOSE_ORG_MODAL:
            return {
                ...state,
                isModalOpen: false,
                id_organization:null,
                name: '',
                address: '',
                INN: ''
            };
        case ActionTypes.OPEN_ORG_MODAL_FOR_DELETE:
            return {
                ...state,
                isModalOpen: true,
                isTypeModalNumber: action.payload.isTypeModalNumber,
                id_organization: action.payload.id_organization,
            };
        case ActionTypes.CHANGE_ORG_NAME:
            return {
                ...state,
                name: action.payload,
            };
        case ActionTypes.CHANGE_ORG_ADDRESS:
            return {
                ...state,
                address: action.payload,
            };
        case ActionTypes.CHANGE_ORG_INN:
            return {
                ...state,
                INN: action.payload,
            };
            case ActionTypes.OPEN_ORG_MODAL_FOR_EDIT:
            return {
                ...state,
                isModalOpen: true,
                id_organization: action.payload.id,
                name: action.payload.name,
                address: action.payload.address,
                INN: action.payload.INN,
                isTypeModalNumber: action.payload.isTypeModalNumber
            };
    }
    return state;
}

