import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/modalActions/employeeModalWindowActions/employeeModalWindowConsts';

/**
 * Состояние для Redux хранилища (стора).
 * @prop {boolean} isModalOpen Состояние открытия модального окна.
 * @prop {boolean} isTypeModalNumber  Состояние открытия модального окна в зависимости от нужной операции.
 */
export interface IStoreState {
    isModalOpen: boolean;
    isTypeModalNumber: number;
    id_employee: number;
    id_division: string;
    FIO: string;
    address: string;
    position: string;
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreState {
        return {
            isModalOpen: false,
            isTypeModalNumber: null,
            id_employee: null,
            id_division: '',
            FIO: '',
            address: '',
            position:''
        };
    }
};

export function employeeModalReducer(state: IStoreState = initialState.state, action: IActionType) {
    switch (action.type) {
        case ActionTypes.OPEN_EMP_MODAL:
            return {
                ...state,
                isModalOpen: true,
                id_division: action.payload.id_division,
                isTypeModalNumber: action.payload.isTypeModalNumber
            };
        case ActionTypes.CLOSE_EMP_MODAL:
            return {
                ...state,
                isModalOpen: false,
                id_employee: null,
                FIO: '',
                address: '',
                position: '',
            };
            case ActionTypes.OPEN_EMP_MODAL_FOR_DELETE:
            return {
                ...state,
                isModalOpen: true,
                isTypeModalNumber: action.payload.isTypeModalNumber,
                id_employee: action.payload.id_employee,
                id_division: action.payload.id_division,
            };
        case ActionTypes.CHANGE_EMP_FIO:
            return {
                ...state,
                FIO: action.payload,
            };
        case ActionTypes.CHANGE_EMP_ADDRESS:
            return {
                ...state,
                address: action.payload,
            };
        case ActionTypes.CHANGE_EMP_POSITION:
            return {
                ...state,
                position: action.payload,
            };
            case ActionTypes.OPEN_EMP_MODAL_FOR_EDIT:
            return {
                ...state,
                isModalOpen: true,
                id_employee: action.payload.id,
                FIO: action.payload.FIO,
                address: action.payload.address,
                position: action.payload.position,
                isTypeModalNumber: action.payload.isTypeModalNumber
            };
    }
    return state;
}

