import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/modalActions/divisionModalWindowActions/divisionModalWindowConsts';

/**
 * Состояние для Redux хранилища (стора).
 * @prop {boolean} isModalOpen Состояние открытия модального окна.
 * @prop {boolean} isTypeModalNumber  Состояние открытия модального окна в зависимости от нужной операции(добавление, изменения или удаления подразделения).
 */
export interface IStoreState {
    isModalOpen: boolean;
    isTypeModalNumber: number;
    id_organization: number;
    id_division: number;
    name: string;
    phone: string;
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreState {
        return {
            isModalOpen: false,
            isTypeModalNumber: null,
            id_organization: null,
            id_division: null,
            name: '',
            phone: ''
        };
    }
};

export function divisionModalReducer(state: IStoreState = initialState.state, action: IActionType) {
    switch (action.type) {
        case ActionTypes.OPEN_DIV_MODAL_FOR_ADD:
            return {
                ...state,
                isModalOpen: true,
                id_organization: action.payload.id_organization,
                isTypeModalNumber: action.payload.isTypeModalNumber
            };
        case ActionTypes.CLOSE_DIV_MODAL:
            return {
                ...state,
                isModalOpen: false,
                name: '',
                phone: ''
            };
        case ActionTypes.OPEN_DIV_MODAL_FOR_DELETE:
            return {
                ...state,
                isModalOpen: true,
                isTypeModalNumber: action.payload.isTypeModalNumber,
                id_organization: action.payload.id_organization,
                id_division: action.payload.id_division,
            };
        case ActionTypes.CHANGE_DIV_NAME:
            return {
                ...state,
                name: action.payload,
            };
        case ActionTypes.CHANGE_DIV_PHONE:
            return {
                ...state,
                phone: action.payload,
            };
            case ActionTypes.OPEN_DIV_MODAL_FOR_EDIT:
            return {
                ...state,
                isModalOpen: true,
                id_division: action.payload.id,
                name: action.payload.name,
                phone: action.payload.phone,
                isTypeModalNumber: action.payload.isTypeModalNumber
            };
    }
    return state;
}

