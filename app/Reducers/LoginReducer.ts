import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/LoginActions/loginConsts';

/**
 * Состояние для Redux хранилища (стора).
 * @prop {boolean} isAuthorizeStatus Состояние авторизации.
 */
export interface IStoreState {
    login: string;
    password: string;
    isAuthorizeStatus: boolean;
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreState {
        return {
            login:'',
            password:'',
            isAuthorizeStatus:false
        };
    }
};

export function LoginReducer(state: IStoreState = initialState.state, action: IActionType) {
    switch (action.type) {
        case ActionTypes.CHANGE_LOGIN:
            return {
                ...state,
        login: action.payload,
            };
        case ActionTypes.CHANGE_PASSWORD:
            return {
                ...state,
                password: action.payload,
            };
            case ActionTypes.AUTHORIZE_INN:
            return {
                ...state,
                isAuthorizeStatus: true,
                login:'',
                password:'',
            };
        case ActionTypes.AUTHORIZE_OUT:
            return {
                ...state,
                isAuthorizeStatus: false,
            };
    }
    return state;
}

