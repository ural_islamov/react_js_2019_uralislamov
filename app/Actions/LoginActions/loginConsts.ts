/**
 * Типы экшенов, используемые в приложении.
 * CHANGE_LOGIN - Прокидывание логина в стор.
 * CHANGE_PASSWORD - Прокидывание пароля в стор.
 * AUTHORIZE_INN - Запрос авторизации на сервер.
 * AUTHORIZE_OUT - Изменение статуса авторизации на false.
 */
export enum ActionTypes {
    CHANGE_LOGIN = 'CHANGE_LOGIN',
    CHANGE_PASSWORD = 'CHANGE_PASSWORD',
    AUTHORIZE_INN = 'AUTHORIZE_INN',
    AUTHORIZE_OUT = 'AUTHORIZE_OUT',
}


