import * as axios from 'axios';
import {Dispatch} from 'redux';
import {IActionType} from '../../common';
import {ActionTypes, AsyncActionTypes} from './loginConsts';
import {ILoginData} from '../Models';

/**
 * Экшены для приложения.
 */
export class LoginActions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }
    changeLogin=(login:any)=>{
        this.dispatch({type: ActionTypes.CHANGE_LOGIN, payload:login})
    };
    changePassword=(password:any)=>{
        this.dispatch({type: ActionTypes.CHANGE_PASSWORD, payload:password})
    };
    outOnWebsite=()=>{
        this.dispatch({type: ActionTypes.AUTHORIZE_OUT})
    };
    getAuthorization=(body:any)=>{
        axios.post('http://localhost:8080/athorize',body).then(response =>{
            if(response.data.isLogin){
                this.dispatch({type: ActionTypes.AUTHORIZE_INN})}
    })}
}
