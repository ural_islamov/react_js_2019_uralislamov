import * as axios from 'axios';
import {Dispatch} from 'redux';
import {IActionType} from '../../../common';
import {ActionTypes, AsyncActionTypes} from './organizationModalWindowConsts';
import {ILoginData} from '../../Models';

/**
 * Экшены для приложения.
 */
export class OrganizationModalWindowActions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }

    openOrgModalForAdd = (isTypeModalNumber:number) => {
        this.dispatch({type: ActionTypes.OPEN_ORG_MODAL_FOR_ADD, payload:isTypeModalNumber})
    };
    closeOrgModal= () => {
        this.dispatch({type: ActionTypes.CLOSE_ORG_MODAL});
    };
    openOrgModalForDelete = (body:any) => {
        this.dispatch({type: ActionTypes.OPEN_ORG_MODAL_FOR_DELETE, payload:body});
    };
    changeOrgName=(body:any)=>{
        this.dispatch({type: ActionTypes.CHANGE_ORG_NAME, payload:body})
    };
    changeOrgAddress=(body:any)=>{
        this.dispatch({type: ActionTypes.CHANGE_ORG_ADDRESS, payload:body})
    };
    changeOrgINN=(body:any)=>{
        this.dispatch({type: ActionTypes.CHANGE_ORG_INN, payload:body})
    };
    getOrganization = (body:any)=>{
        axios.get('http://localhost:8080/organization').then(response =>{
     response.data.forEach((data:any) => {
         if(data.id === body.id_organization){
             data.isTypeModalNumber = body.isTypeModalNumber;
             this.dispatch({type: ActionTypes.OPEN_ORG_MODAL_FOR_EDIT, payload:data})
         }
     })
        });
    };

}
