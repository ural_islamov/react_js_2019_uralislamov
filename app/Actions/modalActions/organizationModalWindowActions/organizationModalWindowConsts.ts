/**
 * Типы экшенов, используемые в приложении.
 * OPEN_ORG_MODAL_FOR_ADD - Открытие модального окна для добавления организации.
 * CHANGE_ORG_NAME - Изменение названия организации.
 * CHANGE_ORG_ADDRESS - Изменение адреса организации.
 * CHANGE_ORG_INN - Изменение ИНН ОГРАНИЗАЦИИ.
 * CLOSE_ORG_MODAL - Закрытие модального окна.
 * OPEN_ORG_MODAL_FOR_EDIT - Открытие модального окна для изменения данных организации.
 * OPEN_ORG_MODAL_FOR_DELETE - Открытие модального окна для запроса подтверждения удаления.
 */
export enum ActionTypes {
    OPEN_ORG_MODAL_FOR_ADD = 'OPEN_ORG_MODAL_FOR_ADD',
    CHANGE_ORG_NAME = 'CHANGE_ORG_NAME',
    CHANGE_ORG_ADDRESS='CHANGE_ORG_ADDRESS',
    CHANGE_ORG_INN='CHANGE_ORG_INN',
    CLOSE_ORG_MODAL='CLOSE_ORG_MODAL',
    OPEN_ORG_MODAL_FOR_EDIT='OPEN_ORG_MODAL_FOR_EDIT',
    OPEN_ORG_MODAL_FOR_DELETE='OPEN_ORG_MODAL_FOR_DELETE'
}


