import * as axios from 'axios';
import {Dispatch} from 'redux';
import {IActionType} from '../../../common';
import {ActionTypes, AsyncActionTypes} from './employeeModalWindowConsts';
import {ILoginData} from '../../Models';

/**
 * Экшены для приложения.
 */
export class EmployeeModalWindowActions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }

    openEmpModal = (body:any) => {
        this.dispatch({type: ActionTypes.OPEN_EMP_MODAL, payload:body})
    };
    closeEmpModal= () => {
        this.dispatch({type: ActionTypes.CLOSE_EMP_MODAL});
    };
    openEmpModalForDelete = (body:any) => {
        this.dispatch({type: ActionTypes.OPEN_EMP_MODAL_FOR_DELETE, payload:body});
    };
    changeEmpFIO=(body:any)=>{
        this.dispatch({type: ActionTypes.CHANGE_EMP_FIO, payload:body})
    };
    changeEmpAddress=(body:any)=>{
        this.dispatch({type: ActionTypes.CHANGE_EMP_ADDRESS, payload:body})
    };
    changeEmpPosition=(body:any)=>{
        this.dispatch({type: ActionTypes.CHANGE_EMP_POSITION, payload:body})
    };
    getEmployee = (body:any)=>{
        axios.get('http://localhost:8080/employee?id='+body.id_division).then(response =>{
     response.data.forEach((data:any) => {
         if(data.id === body.id_employee){
             data.isTypeModalNumber = body.isTypeModalNumber;
             this.dispatch({type: ActionTypes.OPEN_EMP_MODAL_FOR_EDIT, payload:data})
         }
     })
        });
    };

}
