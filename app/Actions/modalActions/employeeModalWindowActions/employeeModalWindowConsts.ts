/**
 * Типы экшенов, используемые в приложении.
 * OPEN_EMP_MODAL - Открытие модального окна.
 * CHANGE_EMP_FIO - Изменение ФИО сотрудника.
 * CHANGE_EMP_ADDRESS - Изменение адреса сотрудника.
 * CHANGE_EMP_POSITION - Изменение должности сотрудника.
 * CLOSE_EMP_MODAL - Закрытие модального окна.
 * OPEN_EMP_MODAL_FOR_EDIT - Открытие модального окна для изменения данных сотрудника.
 * OPEN_EMP_MODAL_FOR_DELETE - Открытие модального окна для запроса подтверждения удаления.
 .
 */
export enum ActionTypes {
    OPEN_EMP_MODAL = 'OPEN_EMP_MODAL',
    CHANGE_EMP_FIO = 'CHANGE_EMP_FIO',
    CHANGE_EMP_ADDRESS='CHANGE_EMP_ADDRESS',
    CHANGE_EMP_POSITION='CHANGE_EMP_POSITION',
    CLOSE_EMP_MODAL='CLOSE_EMP_MODAL',
    OPEN_EMP_MODAL_FOR_EDIT='OPEN_EMP_MODAL_FOR_EDIT',
    OPEN_EMP_MODAL_FOR_DELETE='OPEN_EMP_MODAL_FOR_DELETE',

}


