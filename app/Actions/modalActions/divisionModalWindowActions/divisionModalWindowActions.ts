import * as axios from 'axios';
import {Dispatch} from 'redux';
import {IActionType} from '../../../common';
import {ActionTypes, AsyncActionTypes} from './divisionModalWindowConsts';
import {ILoginData} from '../../Models';

/**
 * Экшены для приложения.
 */
export class DivisionModalWindowActions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }

    openDivModalForAdd = (body:any) => {
        this.dispatch({type: ActionTypes.OPEN_DIV_MODAL_FOR_ADD, payload:body})
    };
    closeDivModal = () => {
        this.dispatch({type: ActionTypes.CLOSE_DIV_MODAL});
    };
    openDivModalForDelete = (body:any) => {
        this.dispatch({type: ActionTypes.OPEN_DIV_MODAL_FOR_DELETE, payload:body});
    };
    changeDivName =(body:any)=>{
        this.dispatch({type: ActionTypes.CHANGE_DIV_NAME, payload:body})
    };
    changeDivPhone =(body:any)=>{
        this.dispatch({type: ActionTypes.CHANGE_DIV_PHONE, payload:body})
    };
    getDivision = (body:any)=>{
        axios.get('http://localhost:8080/division?id='+body.id_organization).then(response =>{
     response.data.forEach((data:any) => {
         if(data.id === body.id_division){
             data.isTypeModalNumber = body.isTypeModalNumber;
             this.dispatch({type: ActionTypes.OPEN_DIV_MODAL_FOR_EDIT, payload:data})
         }
     })
        });
    };

}
