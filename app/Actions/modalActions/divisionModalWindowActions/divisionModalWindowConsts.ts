/**
 * Типы экшенов, используемые в приложении.
 * OPEN_DIV_MODAL_FOR_ADD - Открытие модального окна для добавления подразделения.
 * CHANGE_DIV_NAME - Изменение имени подразделения.
 * CHANGE_DIV_PHONE - Изменение телефона подразделения.
 * CLOSE_DIV_MODAL - Закрытие модального окна.
 * OPEN_DIV_MODAL_FOR_EDIT - Открытие модального окна для изменения данных подразделения.
 * OPEN_DIV_MODAL_FOR_DELETE - Открытие модального окна для запроса подтверждения удаления.
 */
export enum ActionTypes {
    OPEN_DIV_MODAL_FOR_ADD = 'OPEN_DIV_MODAL_FOR_ADD',
    CHANGE_DIV_NAME = 'CHANGE_DIV_NAME',
    CHANGE_DIV_PHONE='CHANGE_DIV_PHONE',
    CLOSE_DIV_MODAL='CLOSE_DIV_MODAL',
    OPEN_DIV_MODAL_FOR_EDIT='OPEN_DIV_MODAL_FOR_EDIT',
    OPEN_DIV_MODAL_FOR_DELETE='OPEN_DIV_MODAL_FOR_DELETE'
}

