/**
 * Типы экшенов, используемые в приложении.
 * SET_ORGANIZATIONS - Получение списка организаций.
 * NOT_ORGANIZATIONS - Действие, если на сервере еще нет списка организаций.
 */
export enum ActionTypes {
    SET_ORGANIZATIONS = 'SET_ORGANIZATIONS',
    NOT_ORGANIZATIONS = 'NOT_ORGANIZATIONS',
}



