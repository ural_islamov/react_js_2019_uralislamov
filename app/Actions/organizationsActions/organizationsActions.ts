import * as axios from 'axios';
import {Dispatch} from 'redux';
import {IActionType} from '../../common';
import {ActionTypes} from './organizationsConsts';

/**
 * Экшены для приложения.
 */
export class organizationsActions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }
    setOrganizations = ()=>{
        axios.get('http://localhost:8080/organization',).then(response =>{
            if(response.data.length === 0){
                this.dispatch({type: ActionTypes.NOT_ORGANIZATIONS});}
            else{
                this.dispatch({type: ActionTypes.SET_ORGANIZATIONS, payload:response.data})
            }
    });};
    deleteOrganization = (id:any)=>{
        axios.delete('http://localhost:8080/deleteOrganization?id='+id,).then(response =>{
           if(response.status===200){
            this.setOrganizations();}
        });
        };
    createOrganization=(body:any)=>{
        axios.post('http://localhost:8080/createOrganization',body).then(response =>{
            if(response.status===200){
                this.setOrganizations();}
        });
    };
    editOrganization=(body:any)=>{
        axios.put('http://localhost:8080/editOrganization',body).then(response =>{
            if(response.status===200){
                this.setOrganizations();}
        });
    };
}
