import * as axios from 'axios';
import {Dispatch} from 'redux';
import {IActionType} from '../../common';
import {ActionTypes, AsyncActionTypes} from './divisionsConsts';
import {ILoginData} from '../Models';

/**
 * Экшены для приложения.
 */
export class divisionsActions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }
    setDivisions = (id)=>{
        axios.get(`http://localhost:8080/division?id=`+id)
            .then(response => {
                this.dispatch({type: ActionTypes.SET_DIVISIONS, payload:response.data})
            })
    };
    createDivision=(body:any)=>{
        let id = body.id_organization;
        axios.post('http://localhost:8080/createDivision',body).then(response =>{
            if(response.status===200){
                this.setDivisions(id);}
        });
    };
    deleteDivision = (id_division:any, id_organization:any)=>{
        axios.delete('http://localhost:8080/deleteDivision?id='+id_division).then(response =>{
            if(response.status===200){
                this.setDivisions(id_organization);}
        } );
    };
    editDivision=(body:any)=>{
        axios.put('http://localhost:8080/editDivision',body).then(response =>{
            if(response.status===200){
                this.setDivisions(response.data.id_organization);}
        });
    };
}
