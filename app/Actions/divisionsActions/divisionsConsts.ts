/**
 * Типы экшенов, используемые в приложении.
 * SET_DIVISIONS - Получение списка подразделений.

 */
export enum ActionTypes {
    SET_DIVISIONS = 'SET_DIVISIONS',
}


