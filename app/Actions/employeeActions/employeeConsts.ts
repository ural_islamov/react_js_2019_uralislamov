/**
 * Типы экшенов, используемые в приложении.
 * SET_EMPLOYEE - Получение списка сотрудников.
 */
export enum ActionTypes {
    SET_EMPLOYEE = 'SET_EMPLOYEE',
}


