import * as axios from 'axios';
import {Dispatch} from 'redux';
import {IActionType} from '../../common';
import {ActionTypes, AsyncActionTypes} from './employeeConsts';
import {ILoginData} from '../Models';

/**
 * Экшены для приложения.
 */
export class employeeActions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }
    setEmployee = (id)=>{
        axios.get(`http://localhost:8080/employee?id=`+id)
            .then(response => {
                this.dispatch({type: ActionTypes.SET_EMPLOYEE, payload:response.data})
            })
    };
    createEmployee=(body:any)=>{
        let id = body.id_division;
        axios.post('http://localhost:8080/createEmployee',body).then(response =>{
            if(response.status===200){
                this.setEmployee(id);}
        });
    };
    deleteEmployee = (id:any, id_division:any)=>{
        axios.delete('http://localhost:8080/deleteEmployee?id='+id).then(response =>{
            if(response.status===200){
                this.setEmployee(id_division);}
        } );
    };
    editEmployee=(body:any)=>{
        axios.put('http://localhost:8080/editEmployee',body).then(response =>{
            if(response.status===200){
                this.setEmployee(response.data.id_division);}
        });
    };
}
