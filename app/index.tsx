import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {App} from './App/App';
import {appStore} from './Store/Store';
import {BrowserRouter} from 'react-router-dom';

/**
 * Каркас приложения с подключением Redux.
 */
ReactDOM.render(
    <BrowserRouter>
    <Provider store={appStore}>
        <App/>
    </Provider>
    </BrowserRouter>,
    document.getElementById('app')
);
