import * as React from 'react';
import {Actions} from '../Actions/Actions';
import './App.less';
import {Navbar} from '../components/Navbar/Navbar';
import {Route} from 'react-router';
import {OrganizationsContainer} from '../components/Organizations/OrganizationsContainer';
import DivisionsContainer from '../components/Divizions/DivisionsContainer';
import EmployeeContainer from '../components/Employee/EmployeeContainer';
import {NavLink} from 'react-router-dom';
import LoginContainer from '../components/Login/LoginContainer';

/**
 * Пропсы компонента из стора.
 * @prop {boolean} loginStatus Состояние зарегистрированности пользователя.
 * @prop {boolean} waitingForLogin Ожидание завершения процедуры авторизации (завершение логина).
 * @prop {boolean} countResult Результат вычисления.
 * @prop {boolean} counting Выполнение вычисления.
 */
 export interface IStateProps{
    loginStatus: boolean;
    waitingForLogin: boolean;
    countResult: number;
    counting: boolean;
    organizations: any;
    isModalOpen: boolean;
    isModalForEdit: boolean;
    divisions:any;
}

/**
 * Пропсы для передачи экшенов.
 * @prop {Actions} actions Экшены для работы приложения.
 */
export interface IDispatchProps{
    actions: Actions;
    actions2: Actions;
}

/**
 * Итоговые пропсы компонента
 */
type TProps = IStateProps & IDispatchProps;

/**
 * Основной класс приложения.
 */
class App extends React.Component<TProps, {}> {
    render() {
        return (
          <div className="app-wrapper">
              <header className="header">
                  <div className="loginBlock">
                  </div>
              </header>
              <nav className="nav">
                  <Navbar/>
              </nav>
              <div className="content">
                  <Route path="/organizations" render={()=><OrganizationsContainer/>}/>
                  <Route path="/login" render={()=><LoginContainer/>}/>
                  <Route path="/division/:id" render={()=><DivisionsContainer/>}/>
                  <Route path="/employee/:id" render={()=><EmployeeContainer/>}/>
              </div>

            </div>
        );
    }
}

export { App};
